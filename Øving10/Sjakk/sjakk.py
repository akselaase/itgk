from game import Game
from localization import translate, tprint, setlang
import userinput


def handle_cmd(cmd, args):
    if cmd == 'setlang':
        setlang(args[0])


def main():
    game = Game()
    while True:
        print(game.state)

        color = translate(game.get_turn_color(),
                          sourcelang='internal', targetlang='english')
        tprint("{}'s turn. Enter your move: ".format(color))
        res = userinput.get_input()

        if res.type == 'cmd':
            handle_cmd(res.cmd, res.args)
            continue
        elif res.type == 'move' and res.cmd == 'typemove':
            try:
                game.move_type(*res.args)
            except Exception as e:
                print(e)
        elif res.type == 'move' and res.cmd == 'posmove':
            game.move_pos(*res.args)

        state, color, reason = game.get_gamestate()
        if state == 'win':
            color = translate(color, sourcelang='internal',
                              targetlang='english')
            tprint('Congratulations to {} on winning!'.format(color))
            break


if __name__ == '__main__':
    main()
