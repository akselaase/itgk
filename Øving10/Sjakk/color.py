White = 'w'
Black = 'b'


def opposite_color(color):
    if color == White:
        return Black
    elif color == Black:
        return White
    else:
        raise Exception('Invalid color.')
