import sys
from div import Dynamic
from localization import translate, tinput


def str_to_pos(str):
    str = str.strip('( )').replace(',', '').replace(' ', '').lower()
    if len(str) != 2:
        raise Exception('Invalid position string.')
    x = str[0]
    y = str[1]
    if x in 'abcdefgh':
        x = 'abcdefgh'.index(x)
    else:
        try:
            x = int(x) - 1
        except ValueError:
            raise Exception('Failed to parse x-coordinate.')
    try:
        y = int(y) - 1
    except ValueError:
        raise Exception('Failed to parse y-coordinate.')

    if not 0 <= x <= 7:
        raise Exception('x must be in range [1, 8] or [A, H].')
    if not 0 <= y <= 7:
        raise Exception('y must be in range [1, 8].')

    return (x, y)


def get_type_move(parsts):
    ptype = input('Enter the type of the piece to move: ')
    target = str_to_pos(input('Enter the target (X, Y): '))
    return ptype, target


def get_pos_move():
    source = str_to_pos(
        input('Enter the position of the piece to move (X, Y): '))
    target = str_to_pos(input('Enter the target (X, Y): '))
    return source, target


def parse_cmd(line):
    parts = line.split()
    return parts[0], parts[1:]


def parse_move(line):
    to = translate('to')
    if to in line:
        parts = line.split(to)
        piecetype = translate(parts[0].strip(), inverse=True)
        piecedest = str_to_pos(parts[1])
        return 'typemove', (piecetype, piecedest)
    else:
        parts = line.split()
        piecesrc = str_to_pos(parts[0])
        piecedest = str_to_pos(parts[1])
        return 'posmove', (piecesrc, piecedest)


def get_input():
    while True:
        try:
            line = input()
        except EOFError:
            sys.exit()

        res = Dynamic()
        if line.startswith('/'):
            res.type = 'cmd'
            res.cmd, res.args = parse_cmd(line[1:])
        else:
            res.type = 'move'
            res.cmd, res.args = parse_move(line)
        return res


'''
12 gullmynter, én av de veier mer eller mindre enn de andre
3 veiinger til rådighet
Hvilken mynt skiller seg ut, og veier den mer eller mindre enn de andre?
'''
