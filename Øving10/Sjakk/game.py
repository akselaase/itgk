from typing import Tuple, List
from color import White, Black, opposite_color
from state import Gamestate
from piece import Piece
import rules


class Game:
    def __init__(self):
        self.state = Gamestate()

    def get_turn_color(self):
        return White if self.state.get_turn_num() % 2 == 0 else Black

    def move_pos(self, piece_pos: Tuple[int, int], target: Tuple[int, int]):
        # TODO: Validate piece_pos and target.
        # Find the piece at piece_pos, throw if non existent
        # Check if the piece has valid moves to target, or throw
        # Move the piece and update the board state

        piece = self.state.get_piece_at(piece_pos)
        if not target in self.state.get_valid_abs_moves(piece):
            raise Exception('Invalid target for piece {}.'.format(piece.type))

        self._move_piece(piece, target)

    def move_type(self, piece_type: str, target: Tuple[int, int]):
        # TODO: Validate piece_type and target
        # Find all pieces which have valid moves to target.
        # Throw if there are more than one piece or none.
        # Move the piece and update the board state

        pieces = [p for p in self.state.pieces[self.get_turn_color()]
                  if self.state.is_alive(p)]
        pieces = [p for p in pieces if piece_type.lower() == p.type[1:]]
        if len(pieces) == 0:
            raise Exception('No pieces matched type constraint.')
        pieces = [
            p for p in pieces if target in self.state.get_valid_abs_moves(p)]
        if len(pieces) == 0:
            raise Exception('No pieces matched type and target constraint.')
        if len(pieces) > 1:
            raise Exception('More than one piece matched constraints.')

        self._move_piece(pieces[0], target)

    def _move_piece(self, piece: Piece, target: Tuple[int, int]):
        if self.get_turn_color() != piece.color:
            raise Exception('Not your turn')

        self.state.move_piece(piece, target)

        opponent = opposite_color(piece.color)
        if self.state.is_in_check(opponent):
            print(opponent, 'is in check.')
            if self.state.is_checkmate(opponent):
                print(opponent, 'is checkmated.')

    def is_in_check(self, color):
        return self.state.is_in_check(color)

    def is_checkmated(self, color):
        return self.is_in_check(color) and self.state.is_checkmate(color)

    def get_gamestate(self):
        # TODO: Check all win conditions, and return
        # the current state of the game, along with an
        # explanation of the condition that leads to that
        # state.
        if self.is_checkmated(White):
            return ('win', 'b', 'checkmate')
        if self.is_checkmated(Black):
            return ('win', 'w', 'checkmate')
        # if self.is_stalemate(White):
        #     return ('draw', 'b', 'stalemate')
        # if self.is_stalemate(Black):
        #     return ('draw', 'w', 'stalemate')
        #

        return ('playing', '', '')
