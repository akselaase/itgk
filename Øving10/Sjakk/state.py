from typing import List, Tuple
from localization import translate
from color import White, Black, opposite_color
from piece import Piece, Pawn, Rook, Knight, Bishop, Queen, King
import rules
import gamelog as log


class Gamestate:
    def __init__(self):
        log.info('Gamestate init')
        self.turn = 0
        self.pieces = {
            White: self.__gen_std_pieces(White),
            Black: self.__gen_std_pieces(Black)
        }
        self.history = []

    def __gen_std_pieces(self, color):
        backrow = 0 if color == White else 7
        frontrow = 1 if color == White else 6
        pieces = [King(color).init_pos((4, backrow))] + \
            [Queen(color).init_pos((3, backrow))] + \
            [Bishop(color).init_pos((x, backrow)) for x in [2, 5]] + \
            [Knight(color).init_pos((x, backrow)) for x in [1, 6]] + \
            [Rook(color).init_pos((x, backrow)) for x in [0, 7]] + \
            [Pawn(color).init_pos((x, frontrow)) for x in range(8)]
        return pieces

    def get_turn_num(self):
        return self.turn

    def get_last_move(self):
        if len(self.history) == 0:
            return None
        else:
            return self.history[-1]

    def exists_piece_at(self, pos: Tuple[int, int]) -> bool:
        assert rules.is_on_map(pos)
        return self.get_piece_at(pos) is not None

    def get_piece_at(self, pos: Tuple[int, int]) -> Piece:
        assert rules.is_on_map(pos)
        for piece in self.pieces[White] + self.pieces[Black]:
            if piece.position == pos:
                return piece
        return None

    def is_alive(self, piece: Piece) -> bool:
        return rules.is_on_map(piece.position)

    def get_all_abs_moves(self, piece: Piece):
        assert isinstance(piece, Piece)
        pos = piece.position
        for move in piece.get_possible_rel_moves():
            target = (pos[0] + move[0], pos[1] + move[1])
            yield target

    def get_valid_abs_moves(self, piece: Piece):
        assert isinstance(piece, Piece)
        for target in self.get_all_abs_moves(piece):
            if rules.is_valid_move(piece, target, self):
                yield target

    def move_piece(self, piece: Piece, target: Tuple[int, int], check_rules=True):
        if check_rules:
            assert rules.is_valid_move(piece, target, self)
        undo_actions = []
        relmove = (target[0] - piece.position[0],
                   target[1] - piece.position[1])

        # Handle the ordinary part of the move
        loser = self.get_piece_at(target)
        if loser is not None:
            undo_actions.append(('move', loser, loser.get_state(), None))
            loser.set_pos((-1, -1))
        undo_actions.append(('move', piece, piece.get_state(), relmove))
        piece.set_pos(target)

        # Check for pawn promotion
        if rules.is_promotion(piece, target):
            undo_actions.append(('move', piece, piece.get_state(), None))
            piece.set_pos((8, 8))
            new_piece = Queen(piece.color)
            new_piece.init_pos(target)
            self.pieces[piece.color].append(new_piece)
            undo_actions.append(('promotion', piece, new_piece))

        # Check for en passant
        if rules.is_en_passant(piece, target, self):
            loser = None
            for evtype, evpiece, _, *args in self.history[-1]:
                if evtype == 'move' and 'pawn' in evpiece.type and \
                        evpiece.color == opposite_color(piece.color) and len(args) > 0:
                    loser = evpiece
                    break
            assert (loser is not None)
            undo_actions.append(('move', loser, loser.get_state(), None))
            loser.set_pos((-1, -1))
        self.turn += 1
        self.history.append(undo_actions)

    def undo(self, count: int = 1):
        for actions in self.history[-count:][::-1]:
            for movetype, piece, arg, *args in actions[::-1]:
                if movetype == 'move':
                    piece.set_state(arg)
                elif movetype == 'promotion':
                    self.pieces[piece.color].remove(arg)
            self.turn -= 1
        del self.history[-count:]

    def is_in_check(self, color) -> bool:
        assert rules.is_valid_color(color)
        king = self.pieces[color][0]
        for other in self.pieces[opposite_color(color)]:
            if king.position in self.get_all_abs_moves(other):
                if rules.is_valid_move(other, king.position, self):
                    return True
        return False

    def is_checkmate(self, color) -> bool:
        assert rules.is_valid_color(color)
        if not self.is_in_check(color):
            return False
        for piece in self.pieces[color]:
            for move in self.get_valid_abs_moves(piece):
                saved = False
                self.move_piece(piece, move)
                if not self.is_in_check(color):
                    saved = True
                self.undo(1)
                if saved:
                    return False
        return True

    def get_grid(self):
        grid = [[None for x in range(8)] for y in range(8)]
        for piece in self.pieces[White] + self.pieces[Black]:
            pos = piece.position
            if not rules.is_on_map(pos):
                continue
            assert grid[pos[1]][pos[0]] is None
            grid[pos[1]][pos[0]] = piece
        return grid

    def __str__(self):
        double_sided = False
        res = '  A B C D E F G H\n' if double_sided else ''
        for index, row in enumerate(self.get_grid()[::-1]):
            linenr = str(8 - index)
            line = linenr
            for piece in row:
                if piece is not None:
                    line += ' ' + translate(piece.type, targetlang='internal')
                else:
                    line += '  '
            res += line + ' ' + (linenr + '\n' if double_sided else '\n')
        res += '  A B C D E F G H'
        return res
