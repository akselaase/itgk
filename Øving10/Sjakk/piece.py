from typing import Tuple
from color import White, Black


class Piece:
    def __init__(self, name: str, color):
        self.name = name
        self.color = color
        self.type = self.color + self.name.lower()
        self.position = (0, 0)
        self.distance = 0

    def get_possible_rel_moves(self):
        return []

    def init_pos(self, pos: Tuple[int, int]):
        self.position = pos
        return self

    def set_pos(self, pos: Tuple[int, int]):
        delta = abs(pos[0] - self.position[0]) + abs(pos[1] - self.position[1])
        self.distance += delta
        self.position = pos

    def get_state(self):
        return [self.position, self.distance]

    def set_state(self, params):
        self.position, self.distance = params

    def __str__(self):
        return self.name

    def __repr__(self):
        return '{:s}@({:d},{:d})'.format(self.type, *self.position)


class Pawn(Piece):
    def __init__(self, color):
        name = 'Pawn'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        sign = 1 if self.color == White else -1
        return [(0, sign), (0, sign * 2),
                (-1, sign), (1, sign)]


class Knight(Piece):
    def __init__(self, color):
        name = 'Knight'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        return [(1, 2), (2, 1), (2, -1), (1, -2),
                (-1, -2), (-2, -1), (-2, 1), (-1, 2)]


class Bishop(Piece):
    def __init__(self, color):
        name = 'Bishop'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        return [(n, n) for n in range(-7, 8)] + \
               [(-n, n) for n in range(-7, 8)]


class Rook(Piece):
    def __init__(self, color):
        name = 'Rook'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        return [(0, n) for n in range(-7, 8)] + \
               [(n, 0) for n in range(-7, 8)]


class Queen(Piece):
    def __init__(self, color):
        name = 'Queen'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        # Bishop moves + Rook moves
        return [(n, n) for n in range(-7, 8)] + \
               [(-n, n) for n in range(-7, 8)] + \
               [(0, n) for n in range(-7, 8)] + \
               [(n, 0) for n in range(-7, 8)]


class King(Piece):
    def __init__(self, color):
        name = 'King'
        Piece.__init__(self, name, color)

    def get_possible_rel_moves(self):
        return [(0, 1), (1, 1), (1, 0), (1, -1)] + \
               [(0, -1), (-1, -1), (-1, 0), (-1, 1)]
