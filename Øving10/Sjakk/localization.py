def dict_to_map(dictionary):
    forward, backward = dict(), dict()
    for key, value in dictionary.items():
        forward[key] = value
        backward[value] = key
    return forward, backward


def map_to_lang(forward, backward):
    def map_translator(phrase, inverse=False):
        if inverse:
            if phrase in backward:
                return backward[phrase]
        else:
            if phrase in forward:
                return forward[phrase]
        return None
    return map_translator


def dict_to_lang(dictionary):
    forward, backward = dict_to_map(dictionary)
    return map_to_lang(forward, backward)


def identity(phrase: str, inverse=False):
    return phrase


languages = {
    'english': identity,
}


def load_language(name):
    forward = dict()
    backward = dict()
    with open('lang/{}.txt'.format(name), 'r') as file:
        for line in file:
            if line.isspace():
                continue
            key, val = line.split('--', maxsplit=1)
            if len(key) < 2 or len(val) < 2:
                continue
            b = (key[-1] == '<')
            f = (val[0] == '>')
            key, val = key.strip(' <'), val.strip(' >\n')
            if f:
                forward[key] = val
            if b:
                backward[val] = key
    languages[name] = map_to_lang(forward, backward)


load_language('internal')
load_language('norsk')
currlang = 'english'


def setlang(lang):
    global currlang

    if lang not in languages:
        raise Exception("Don't know nothing about {}".format(lang))

    currlang = lang
    tprint('Set {} as default language.'.format(lang))


def get_translator(sourcelang, targetlang):
    def nonefunc(x: str, y: bool = False):
        return None

    if sourcelang not in languages or targetlang not in languages:
        return nonefunc
    else:
        def translator(phrase: str, inverse: bool = False):
            strans = languages[sourcelang]
            ttrans = languages[targetlang]
            if inverse:
                return strans(ttrans(phrase, True))
            else:
                return ttrans(strans(phrase, True))
        return translator


def translate(phrase: str, sourcelang: str = None, targetlang: str = None, inverse: bool = False, ignore_errors=False):
    if sourcelang is None:
        sourcelang = 'english'
    if targetlang is None:
        targetlang = currlang
    if inverse:
        sourcelang, targetlang = targetlang, sourcelang

    translator = get_translator(sourcelang, targetlang)
    res = translator(phrase)

    if res is None:
        if ignore_errors:
            return phrase
        else:
            raise Exception(
                "No translation existed for '{}' from '{}' to '{}'.".format(phrase, sourcelang, targetlang))
    else:
        return res


def tprint(fmt, *args, **kwargs):
    sourcelang = None
    targetlang = None
    if 'sourcelang' in kwargs:
        sourcelang = kwargs[sourcelang]
        del kwargs[sourcelang]
    if 'targetlang' in kwargs:
        targetlang = kwargs[targetlang]
        del kwargs[targetlang]
    fmt = translate(fmt, sourcelang=sourcelang,
                    targetlang=targetlang, ignore_errors=True)
    args = [translate(arg, sourcelang=sourcelang, targetlang=targetlang, ignore_errors=True)
            for arg in args]
    print(fmt, *args, **kwargs)


def tinput(prompt):
    prompt = translate(prompt)
    return input(prompt)
