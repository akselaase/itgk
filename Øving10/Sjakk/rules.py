from typing import Tuple
from color import White, Black, opposite_color
from piece import Piece


def _sgn(val):
    if val > 0:
        return 1
    if val < 0:
        return -1
    return 0


def is_valid_color(color):
    return color == White or color == Black


def is_on_map(pos: Tuple[int, int]):
    x, y = pos
    return (0 <= x <= 7) and (0 <= y <= 7)


def is_promotion(piece: Piece, target: Tuple[int, int]):
    if not 'pawn' in piece.type:
        return False
    if piece.color == White and target[1] == 7 or piece.color == Black and target[1] == 0:
        return True
    return False


def is_en_passant(piece: Piece, target: Tuple[int, int], state):
    if not 'pawn' in piece.type:
        return False

    lastmove = state.get_last_move()
    if lastmove is None:
        return False

    # Check if the last move was a pawn that moved two steps ahead
    # and $piece is a pawn able to intercept him.
    for evtype, evpiece, _, *args in lastmove:
        if evtype == 'move' and 'pawn' in evpiece.type and \
                evpiece.color == opposite_color(piece.color) and len(args) > 0:
            relmove = args[0]
            if abs(relmove[1]) == 2:
                middle_pos = (
                    evpiece.position[0], evpiece.position[1] - relmove[1] // 2)
                if target == middle_pos:
                    return True
    return False


def is_valid_position(pos: Tuple[int, int]):
    return is_on_map(pos) or pos == (-1, -1)


def _is_valid_pawn_move(pawn: Piece, target: Tuple[int, int], state):
    # TODO: Implement en passant
    # Notice: Not reimplementing checks done in is_valid_move.

    x_dist, y_dist = target[0] - pawn.position[0], target[1] - pawn.position[1]

    is_attacking = (x_dist != 0)
    target_is_occupied = state.exists_piece_at(target)
    if is_attacking and not target_is_occupied and not is_en_passant(pawn, target, state):
        return False
    if not is_attacking and target_is_occupied:
        return False

    is_moving_two = abs(y_dist) == 2
    if is_moving_two and pawn.distance > 0:
        return False
    middle_pos = (pawn.position[0], pawn.position[1] + y_dist // 2)
    if is_moving_two and \
            (target_is_occupied or state.exists_piece_at(middle_pos)):
        return False

    return True


def _is_line_empty(piece: Piece, target: Tuple[int, int], state):
    rel_x = target[0] - piece.position[0]
    rel_y = target[1] - piece.position[1]
    assert rel_x == 0 or rel_y == 0 or abs(rel_x) == abs(rel_y)

    steps = max(abs(rel_x), abs(rel_y))
    inc_x, inc_y = _sgn(rel_x), _sgn(rel_y)
    for s in range(1, steps):
        x = piece.position[0] + s * inc_x
        y = piece.position[1] + s * inc_y
        if state.exists_piece_at((x, y)):
            return False
    return True


def _is_valid_king_move(king: Piece, target: Tuple[int, int], state):
    # TODO: I don't like mutating the state here (even just temporarily),
    # so I want to change that.
    # TODO: Implement castling. Probably needs to be implemented as something
    # separate to normal moves.

    state.move_piece(king, target, check_rules=False)
    in_check = state.is_in_check(king.color)
    state.undo()
    return not in_check


def _puts_king_in_check(piece: Piece, target: Tuple[int, int], state):
    state.move_piece(piece, target, check_rules=False)
    in_check = state.is_in_check(piece.color)
    state.undo()
    return in_check


def is_valid_move(piece: Piece, target: Tuple[int, int], state):
    # TODO: Implement pawn promotion
    rel_move = (target[0] - piece.position[0], target[1] - piece.position[1])
    if not rel_move in piece.get_possible_rel_moves():
        return False
    if not is_on_map(target):
        return False
    for other in state.pieces[piece.color]:
        if other.position == target:
            return False
    if _puts_king_in_check(piece, target, state):
        return False
    if 'knight' not in piece.type:
        if not _is_line_empty(piece, target, state):
            return False
    if 'pawn' in piece.type:
        return _is_valid_pawn_move(piece, target, state)
    if 'king' in piece.type:
        return _is_valid_king_move(piece, target, state)
    return True


def is_winner(color, state):
    pass
