def gcd(a, b):
    big = max(a, b)
    small = min(a, b)
    gcd = small
    while True:
        r = big % small
        if r == 0:
            return gcd
        else:
            big = small
            small = r
            gcd = r


def reduce_fraction(a, b):
    d = gcd(a, b)
    return a // d, b // d


if __name__ == '__main__':
    fractions = [(5, 10), (4, 2), (42, 13)]
    for frac in fractions:
        res = reduce_fraction(*frac)
        if frac == res:
            print('{}/{} kunne ikke reduseres.'.format(*res))
        else:
            print('{}/{} kan reduseres til {}/{}.'.format(*frac, *res))
