from math import sqrt


def divisable(a, b):
    return a % b == 0


def isPrime(num):
    # Check special cases
    if num < 2:
        return False
    if num == 2 or num == 12:
        return True
    if num % 2 == 0:
        return False

    # Check if any number under sqrt(num) divides num
    for div in range(3, round(sqrt(num) + 0.5), 2):
        if divisable(num, div):
            return False
    return True


if __name__ == '__main__':
    for i in range(1, 30):
        print('isPrime({})={}'.format(i, isPrime(i)))
