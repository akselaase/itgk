from math import inf


def iterative(tolerance):
    prev = -inf
    prod = 1.0
    i = 0
    while prod - prev > tolerance:
        i += 1
        prev = prod
        prod *= 1 + 1 / i ** 2
    return prod, i


def recursive(tolerance, level=1):
    factor = 1 + 1 / level ** 2
    if factor < 1 + tolerance:
        return factor, level - 1
    res, level = recursive(tolerance, level + 1)
    return factor * res, level


if __name__ == '__main__':
    for i in range(6):
        t = 10 ** -i
        print('tol={}'.format(t))
        print('Iterativ: produkt={:.5f}, iterasjoner: {}'.format(
            *iterative(t)))
        print('Rekursiv: produkt={:.5f}, rekursjonsdybde: {}'.format(
            *recursive(t)))
