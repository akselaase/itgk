a) Hvor mange ulike verdier kan representeres med 4 bytes?
(2^8)^4=2^32=4294967296

b) Hvor mange bytes trenger man for å representere et fullt HD bilde (1920x1080) i sort/hvitt?
1920*1080 bytes gitt 8-bits fargedybde. 
Dersom dersom man snakker kun fargene svart/hvitt (altså 1-bit fargedybde)
blir det 1920*1080 bits. Selvfølgelig kan bilder komprimeres,
så dette er ikke absolutt minimum
 
c) Hva er en DAC?  
En Digital to Analog Converter gjør om et digitalt signal til
en analog spenning.

d) Hva er 18 i totallssystemet? Hva er 18 i 16-tallssystemet? 
(18)_10 = (10010)_2
(18)_10 = (12)_16

e) ’OSTE’ kodet i ASCII blir: ’0100 1111 0101 0011 0101 0100 0100 0101’, hva blir ’POP’ kodet i ASCII?
01010000 01001111 01010000

f) Hva er en piksel, og hva er dens funksjon i datamaskinens sammenheng?
En piksel i et digitalt bilde er fargeverdien på et spesifikt punkt i bildet.
Avhengig av hvor mange fargekanaler det er i bildet, og fargedybden for hver kanal
så kan størrelsen på dataen pikselen representerer variere. Dens funksjon er å
kvantisere et bilde som i utgangspunktet besto av kontinuerlige/arbitrære farger,
slik at det kan representeres digitalt.

g) Hva er forskjellen på analog og digital lyd? 
Et analogt signal er kontinuerlig langs tidsaksen, i motsetning til et digitalt signal
som kun kan ta diskrete verdier ved diskrete tidspunkt. 