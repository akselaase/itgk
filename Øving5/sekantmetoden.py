from math import e


def f(x):
    return (x - 12) * e ** (x / 2) - 8 * (x + 2) ** 2


def g(x):
    return -x - 2 * x ** 2 - 5 * x ** 3 + 6 * x ** 4


def differentiate(x_curr, x_prev, func):
    return (func(x_curr) - func(x_prev)) / (x_curr - x_prev)


def secant_method(x0, x1, func, tolerance):
    funcvalue = func(x0)
    while abs(funcvalue) > tolerance:
        x1, x0 = x0, x0 - funcvalue / differentiate(x0, x1, func)
        funcvalue = func(x0)
    return x0, funcvalue


if __name__ == '__main__':
    params = [(11, 13, f, 0.00001),
              (1, 2, g, 0.00001),
              (0, 1, g, 0.00001)]
    for values in params:
        print('Kjører sekantmetoden med følgende parametre:')
        print('x0={}, x1={}, funksjon={.__name__}, toleranse={}'.format(*values))
        print('Og fikk et nullpunkt ved x={:.5f}, func(x)={}'.format(
            *secant_method(*values)))
        print()
