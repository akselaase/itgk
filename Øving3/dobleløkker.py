from math import ceil, sqrt
from random import randint

def a():
    for i in range(5):
        line = ''
        for j in range(i + 1):
            line += str(j + 1) + ' '
        print(line)

def b():
    for i in range(5):
        spaces = ''
        for j in range(i + 1):
            spaces += ' '
        print('#' + spaces + '#')

def factor(num):
    factors = []
    if num == 0:
        factors.append(0)
    if num == 1:
        factors.append(1)

    for factor in range(2, 1 + int(ceil(sqrt(num)))):
        while num % factor == 0:
            factors.append(int(factor))
            num /= factor
    if num != 1:
        factors.append(num)
    factorization = '*'.join([str(x) for x in factors]) 
    print(factorization)

def c():
    while True:
        s = input('Skriv inn et positivt heltall: ')
        if s == '':
            return
        num = int(s)
        factor(num)

def d():
    while True:
        a = randint(0, 9)
        b = randint(0, 9)
        ans = a * b

        print('Hva blir {} * {}?'.format(a, b))
        tries = 3
        while tries > 0:
            x = int(input())
            if x == ans:
                break
            print('Dette var feil. Du har {} forsøk igjen.'.format(tries - 1))
            tries -= 1
        if tries > 0:
            print('Korrekt! Vil du ha flere spørsmål? (J/n)')
            if input() == 'n':
                return
        else:
            print('Dårlig jobba. Vil du har flere spørsmål? (J/n)')
            if input() == 'n':
                return


if __name__ == '__main__':
    a()
    input('Press enter')
    b()
    input('Press enter')
    c()
    d()
