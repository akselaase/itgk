

def sum(r, n, tol=0.001):
    target = 1 / (1 - r)
    S = 0
    power = 0
    while power <= n and abs(S - target) > tol:
        S += pow(r, power)
        power += 1
    return (S, power, S - target)
        
def test():
    params = [
        (0.5, 4, 0.4),
        (0.5, 4, 0.001),
        (0.5, 10, 0.001),
        (0.5, 100, 0.001),
        (0.5, 100, 0.00001),
        (0.5, 100, 0.00000001)
    ]

    for row in params:
        S, iterations, diff = sum(*row)
        print('Med parametrene r={:f}, n={:d} og tol={:f} fikk vi sum={:f} ({:f} feil) etter {:d} iterasjoner.'.format
             (*row, S, diff, iterations))

if __name__ == '__main__':
    test()
