

def altsum(n):
    sign = 1
    S = 0
    for i in range(1, n + 1):
        S += sign * i ** 2
        sign *= -1
    return S


def roofsum(roof):
    sign = 1
    prev = 0
    S = 0
    i = 1
    while True:
        prev = S
        S += sign * i ** 2
        if S > roof:
            print('Siste verdi før summen oversteg {:d} var {:d}, etter {:d} iterasjoner.'.format(roof, prev, i - 1))
            print('Neste verdi var', S)
            break
        sign *= -1
        i += 1

if __name__ == '__main__':
    while True:
        i = input('Nummer på leddet: ')
        if i == '':
            break
        print(altsum(int(i)))

    while True:
        i = input('Øvre grense: ')
        roofsum(int(i))