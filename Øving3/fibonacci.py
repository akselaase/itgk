

def fib(k: int):
    prev = 1
    curr = 0
    S = 0
    while k > 0:
        prev, curr = curr, prev + curr
        k -= 1
        S += curr
    return curr, S


def listfib(k: int):
    return [fib(i)[0] for i in range(k + 1)]


if __name__ == '__main__':
     while True:
        i = int(input('Nummer på fibonaccitallet: '))
        print('F_n={}, sum={}'.format(*fib(i)))
        print('Liste opp til F_n:', listfib(i))
