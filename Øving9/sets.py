
# a
my_set = set()

# b
for i in range(1, 20, 2):
    my_set.add(i)

# c
oddetall_10 = set(n for n in range(1, 10, 2))

# d
a_men_ikke_b = my_set.difference(oddetall_10)

# e
# Jeg forventer en tom mengde ettersom det ikke er noen
# felles elementer i de to mengdene.


# f
def all_unique(l):
    return len(l) == len(set(l))


# g
def remove_duplicates(l):
    return list(set(l))
