
my_family = dict()


def add_family_member(role, name):
    if role in my_family:
        if type(my_family[role]) is list:
            my_family[role].append(name)
        else:
            my_family[role] = [name, my_family[role]]
    else:
        my_family[role] = name


add_family_member('mor', 'Lise')
add_family_member('far', 'Nils')
add_family_member('hund', 'Scott')
add_family_member('hund', 'Arya')
print(my_family)
