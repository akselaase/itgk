import random

class Bruteforce:
    def __init__(self, sequence):
        self.sequence = sequence
        self.lower = min(sequence)
        self.upper = max(sequence)

    def find_permutation(self, start, end):
        while start < end:
            random.seed(start)
            items = list(self.sequence)
            while len(items) > 0:
                num = random.randint(self.lower, self.upper)
                if num != items[0]:
                    break
                else:
                    items.remove(num)
            if len(items) == 0:
                return start, self.lower, self.upper
            else:
                start += 1
        return None

    def find_combination(self, start, end):
        while start < end:
            random.seed(start)
            items = list(self.sequence)
            while len(items) > 0:
                num = random.randint(self.lower, self.upper)
                if not num in items:
                    break
                else:
                    items.remove(num)
            if len(items) == 0:
                return start, self.lower, self.upper
            else:
                start += 1
        return None

