 import sys


class Symbol:
    def __init__(self, string, frequency):
        self.string = string
        self.frequency = frequency

    def __str__(self):
        return '{} {}'.format(self.string, self.frequency)


class BinaryTree:
    def __init__(self, left, right):
        self.left = left
        self.right = right
        self.string = left.string + '&' + right.string
        self.frequency = left.frequency + right.frequency


def huffman(freqs: dict):
    queue = [Symbol(symb, freq) for symb, freq in freqs.items()]
    while len(queue) >= 2:
        queue.sort(key=lambda s: s.frequency)
        right, left = queue[:2]
        del queue[:2]
        new_node = BinaryTree(left, right)
        queue.append(new_node)
        print('Combined {} and {} to a new node with frequency {}.'.format(
            left.string, right.string, new_node.frequency))
    return queue[0]


def printtree(tree: BinaryTree):
    symbols = []
    nodes = [('', tree)]
    while len(nodes) > 0:
        path, node = nodes[0]
        del nodes[0]
        if type(node) is Symbol:
            symbols.append((path, node))
        else:
            nodes.append((path + '0', node.left))
            nodes.append((path + '1', node.right))
    for path, symb in symbols:
        print(symb, path)


def main():
    #lines = ['a 7', 'b 20', 'c 35', 'd 25', 'e 8', 'f 5']
    #lines = ['a 12', 'b 24', 'c 35', 'd 20', 'e 7', 'f 2']
    lines = ['a 22', 'b 10', 'c 35', 'd 9', 'e 1', 'f 35', 'g 15']
    freqs = {}
    for line in lines:
        line = line.split()
        if len(line) != 2:
            continue
        else:
            try:
                freqs[line[0]] = int(line[1])
            except ValueError as err:
                print(err)
    tree = huffman(freqs)
    printtree(tree)


if __name__ == "__main__":
    main()
