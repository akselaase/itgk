import numpy as np


def multiply(l):
    if len(l) == 0:
        return 1
    if len(l) == 1:
        return l[0]
    if len(l) == 2:
        return l[0] * l[1]
    else:
        mid = len(l) // 2
        return multiply(l[:mid]) * multiply(l[mid:])


def gcd(a, b, keepLog=False):
    a, b = max(a, b), min(a, b)
    log = [a] if keepLog else None
    while b != 0:
        a, b = b, a % b
        if keepLog:
            log.append(a)
    if keepLog:
        return a, log
    else:
        return a


def gcdlist(l):
    if len(l) == 0:
        return 1
    if 1 <= len(l) <= 2:
        return gcd(l[0], l[-1])
    else:
        mid = len(l) // 2
        return gcd(gcdlist(l[:mid]), gcdlist(l[mid:]))


def q(n, log):
    return log[n + 1] // log[n]


def r(n, log):
    return log[n + 1] % log[n]


def inv(value, modulus):
    ''' Find x and y such that 
        value * x - modulus * y = 1 
        and return x.'''

    d, a = gcd(value, modulus, keepLog=True)
    if d != 1:
        raise ValueError(
            "The inverse of {} (mod {}) doesn't exist.".format(value, modulus))

    a = list(reversed(a))
    greater = 0
    lesser = 1

    for i in range(1, len(a) - 1):
        big = a[i + 1]
        smol = a[i]
        r = big % smol
        q = big // smol

        greater, lesser = lesser, -q * lesser + greater

    return lesser % modulus


def inverse(value, modulus):
    if gcd(value, modulus) != 1:
        raise ValueError(
            "The inverse of {} (mod {}) doesn't exist.".format(value, modulus))
    # Brute force
    inv = 1
    while (value * inv) % modulus != 1:
        inv += 1
    return inv


class Basis:
    def __init__(self, moduli):
        if len(moduli) < 2:
            raise ValueError(
                'We need at least 2 numbers to do something interesting.')
        if not gcdlist(moduli) == 1:
            raise ValueError('All base numbers must be coprime to each other.')
        self.m = moduli
        self.product = multiply(self.m)
        self.M = [self.product // m for m in self.m]
        self.inv = [inverse(Mi, mi) for Mi, mi in zip(self.M, self.m)]

    def min_value(self):
        return 0

    def max_value(self):
        return self.product - 1

    def to_basis(self, number):
        return np.array([number % mi for mi in self.m])

    def from_basis(self, coordinates):
        if len(self.m) != len(coordinates):
            raise ValueError('Invalid length on coordinates.')
        result = 0
        for i, coord in enumerate(coordinates):
            result += coord * self.M[i] * self.inv[i]
        return result % self.product
