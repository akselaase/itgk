

def a(string):
    for c in string:
        print(c)


def b(string):
    return string[2] if len(string) > 2 else 'q'


def c(string):
    return len(string) - 1


def main():
    a('Discworld')
    print(b('Mistborn'))
    print(b('IT'))
    print(c('The Way of Kings'))
    print(c('Elantris'))


if __name__ == '__main__':
    main()
