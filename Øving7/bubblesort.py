

def bubble_sort_inplace(l):
    unsorted = True
    while unsorted:
        unsorted = False
        for i in range(len(l) - 1):
            if l[i] > l[i + 1]:
                l[i], l[i + 1] = l[i + 1], l[i]
                unsorted = True
    return l


def selection_sort_destructive(l):
    result = list()
    while len(l) > 0:
        item = max(l)
        result.insert(0, item)
        l.remove(item)
    return result


def nmin(iter):
    index = 0
    value = None
    for i, val in enumerate(iter):
        if value is None or val < value:
            index, value = i, val
    if value is None:
        index = -1
    return index, value


def selection_sort_inplace(l):
    for i in range(len(l) - 1):
        ind, val = nmin(l[i:])
        l[i], l[i + ind] = val, l[i]
    return l


import random
orig = list(range(20, 0, -1))
random.shuffle(orig)

# Begge er vel O(n^2)?
