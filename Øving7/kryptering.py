

def to_num(string):
    string = string.encode('utf-8')
    num = 0
    exponent = 0
    for byte in string[::-1]:
        num += byte * 256 ** exponent
        exponent += 1
    return num


def to_string(num):
    exponent = (num.bit_length() + 7) // 8 - 1
    barray = []
    while num > 0:
        quotient = num // 256 ** exponent
        remainder = num % 256 ** exponent
        barray.append(quotient)
        exponent -= 1
        num = remainder
    return str(bytes(barray), 'utf-8')


def encrypt(message, key):
    message = to_num(message)
    key = to_num(key)
    return message ^ key


def decrypt(ciphertext, key):
    key = to_num(key)
    return to_string(ciphertext ^ key)


def main():
    import string
    import random

    keyspace = string.ascii_letters + string.digits + 'æøåÆØÅ'

    while True:
        message = input('Hvilken melding skal krypteres? ')

        keysize = len(message)
        key = ''.join([random.choice(keyspace) for i in range(keysize)])
        encrypted = encrypt(message, key)
        print('Kryptert melding: {} (nøkkel: {})'.format(encrypted, key))

        decrypted = decrypt(encrypted, key)
        print('Dekryptert melding:', decrypted)
        print()

        assert decrypted == message


if __name__ == '__main__':
    main()
