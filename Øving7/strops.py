import re


def all_indices_re(string, substring):
    return [match.start()
            for match in re.finditer(substring.lower(), string.lower())]


def all_indices(string, substring):
    string, substring = string.lower(), substring.lower()
    results = []
    index = 0
    while index < len(string):
        try:
            ind = string.index(substring, index)
            results.append(ind)
            index = ind + 1
        except ValueError:
            break
    return results


def replace(string, substring, replacement):
    result = string
    indices = all_indices(string, substring)
    lensub, lenrep = len(substring), len(replacement)
    ichange = 0

    for i, new in zip(indices, indices[1:] + [indices[-1] + lensub]):
        maxsub = min(lensub, new - i)
        result = result[0:i + ichange] + \
            replacement + \
            result[i + ichange + maxsub:]
        ichange += lenrep - maxsub

    return result


def main():
    string = 'Is this the real life? Is this just fantasy?'
    substring = 'iS'

    print(all_indices(string, substring))

    replacement = 'cool'
    print(replace(string, substring, replacement))

    string = 'Never let you goooo let me goo. Never let me goo oooo'
    substring = 'oo'
    print(replace(string, substring, replacement))


if __name__ == '__main__':
    main()
