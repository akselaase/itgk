

def get_coins(value):
    coins = [20, 10, 5, 1]
    result = {20: 0, 10: 0, 5: 0, 1: 0}
    while value > 0:
        for coin in coins:
            if value >= coin:
                value -= coin
                result[coin] += 1
                break
    return result


def main():
    teeth = [95, 103, 71, 99, 114, 64, 95, 53, 97, 114, 109, 11, 2, 21,
             45, 2, 26, 81, 54, 14, 118, 108, 117, 27, 115, 43, 70, 58, 107]
    for weight in teeth:
        coins = get_coins(weight)
        pretty = ', '.join('{}: {}'.format(
            coin, coins[coin]) for coin in sorted(coins.keys(), reverse=True))
        print('{:>3}: {}'.format(weight, pretty))


if __name__ == '__main__':
    main()
