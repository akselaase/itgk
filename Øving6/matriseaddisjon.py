import random


def random_matrix(n, m):
    return [[random.randint(0, 9)
             for x in range(n)]
            for y in range(m)]


def print_matrix(m, name):
    indent = len(name) + 2

    print('{}=['.format(name))
    for row in m:
        print('{}{}'.format(' ' * indent, row))
    print('{}]'.format(' ' * (indent - 1)))


def matrix_addition(a, b):
    if len(a) == 0:
        return []
    if len(a) != len(b) \
            or len(a[0]) != len(b[0]):
        print('Matrisene er ikke av samme dimensjon.')
        return

    m = len(a)
    n = len(a[0])

    return [[a[y][x] + b[y][x]
             for x in range(n)]
            for y in range(m)]


def main():
    A = random_matrix(4, 3)
    print_matrix(A, 'A')
    B = random_matrix(3, 4)
    print_matrix(B, 'B')
    C = random_matrix(3, 4)
    print_matrix(C, 'C')
    D = matrix_addition(A, B)
    E = matrix_addition(B, C)
    print_matrix(E, 'B+C')


if __name__ == '__main__':
    main()
