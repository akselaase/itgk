import random


def permutation(source, n):
    clone = list(source)
    drawn = []
    for i in range(n):
        value = random.choice(clone)
        drawn.append(value)
        clone.remove(value)
    return drawn


def comp_list(a, b):
    matching = 0
    for val in a:
        if val in b:
            matching += 1
    return matching


def draw_numbers(n=10):
    numbers = list(range(1, 35))
    return permutation(numbers, n)


def winnings(drawn, guess):
    m = comp_list(guess, drawn[:7])
    e = comp_list(guess, drawn[7:])

    if m == 7:
        return 2749455
    if m == 6 and e > 0:
        return 102110
    if m == 6:
        return 3385
    if m == 5:
        return 95
    if m == 4 and e > 0:
        return 45
    return 0


def sim_million():
    price = 5
    cash = 0
    count = 1000000
    for i in range(count):
        row = draw_numbers(n=7)
        game = draw_numbers()
        cash += winnings(game, row) - price
    print('Gjennomsnittlig vinning etter 1 million rader er',
          cash // count, 'kr per rad.')


def main():
    price = 5
    while True:
        num = input('Hvor mange rekker vil du kjøpe ({}kr per)? '.format(price))
        if num == '' or num == 'n' or num == '0':
            break
        num = int(num)

        rows = [draw_numbers(n=7) for i in range(num)]
        print('Rekkene dine:')
        for row in rows:
            print('    ', '  '.join(str(n).rjust(2) for n in row))

        drawn = draw_numbers()
        print('Lottotallene:')
        print(' '.join(str(n) for n in drawn[:7]))
        print('Tilleggstallene:')
        print(' '.join(str(n) for n in drawn[7:]))

        win_sum = sum(winnings(drawn, row) for row in rows)
        if win_sum > 0:
            print('Du vant {} kroner! (men betalte {}).'.format(win_sum, num*price))
        else:
            print('Du vant ikke på noen av radene. {} kroner tapt.'.format(num * price))

    if input('Vil du kjøre simuleringen (J/n)? ').lower() != 'n':
        sim_million()


if __name__ == '__main__':
    main()
