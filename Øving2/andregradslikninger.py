from math import sqrt, isclose


def solve(a, b, c):
    textform = '{:.2f}x²{:+.2f}x{:+.2f}'.format(a, b, c)
    discriminant = b ** 2 - 4 * a * c
    frac1 = -b / (2 * a)
    if discriminant > 0:
        frac2 = sqrt(discriminant) / (2 * a)
        tmp1 = frac1 + frac2
        tmp2 = frac1 - frac2
        if abs(tmp1) < abs(tmp2):
            x1 = c / (a * tmp2)
            x2 = tmp2
        else:
            x1 = tmp1
            x2 = c / (a * tmp1)

        print('Andregradslikningen {:s} har to reelle løsninger:'.format(textform))
        print('x = {:+.2E}, og'.format(x1))
        print('x = {:+.2E}'.format(x2))
    elif isclose(discriminant, 0):
        print('Andregradslikningen {:s} har én reell løsning:'.format(textform))
        print('x = {:+.2E}'.format(frac1))
    else:
        print('Andregradslikningen {:s} har kun komplekse løsninger.'.format(textform))


def main():
    while True:
        a = float(input('a: '))
        b = float(input('b: '))
        c = float(input('c: '))
        solve(a, b, c)

    
if __name__ == '__main__':
    main()