

def get_price():
    days_to_travel = int(input('Dager til du skal reise? '))

    eligible_for_miniprice = days_to_travel > 14
    wants_miniprice = False
    base_price = 440

    if eligible_for_miniprice:
        print('Minipris 199,- kan ikke refunderes/endres')
        wants_miniprice = input('Ønskes dette (J/N)? ').lower() == 'j'
        if wants_miniprice:
            base_price = 199
        
    percentage = 1
    student = input('Er du student (J/N)? ').lower() == 'j'
    if student:
        if wants_miniprice:
            percentage = 0.90
        else:
            percentage = 0.75

    if not wants_miniprice:
        age = int(input('Skriv inn din alder: '))
        if age < 16:
            percentage *= 0.50
        elif age >= 60:
            percentage *= 0.75
        else:
            military = input('Er du på militær tjenestereise (J/N)? ').lower() == 'j'
            if military:
                percentage *= 0.75
                
    print('Prisen på billetten din blir: {:.0f}'.format(base_price * percentage))


if __name__ == '__main__':
    while True:
        get_price()