

def farge(posisjon: str):
    if len(posisjon) != 2:
        print('Feil input. Du må skrive akkurat to tegn\n')
    else:
        col = posisjon[0].lower()
        row = posisjon[1]
        if col not in 'abcdefgh' or row not in '12345678':
            print('Feil input.\nFørste tegn må være en bokstav A-H eller a-h\nAndre tegn må være et tall 1-8')
        else:
            col = ord(col) - ord('a')
            row = int(row)
            return 'Svart' if (row + col) % 2 == 1 else 'Hvit'

def main():
    while True:
        pos = input('Posisjon: ')
        print(farge(pos))


if __name__ == '__main__':
    main()