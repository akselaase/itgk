
def allmonths():
    return ['januar', 'februar', 'mars', 'april', 'mai', 'juni', 'juli', 'august', 'september', 'oktober', 'november', 'desember']


def årstid(month: str, day: int):
    months = allmonths()
    seasons = ['vinter', 'vinter',     20,   'vår', 'vår',    21, 'sommer', 'sommer',       22,    'høst',   'høst',      21]

    monthindex = months.index(month.lower())
    season = seasons[monthindex]
    if type(season) is str:
        return season
    else:
        return seasons[(monthindex - 1) % 12] if day < season else \
               seasons[(monthindex + 1) % 12] 


def main():
    while True:
        month = input("Måned: ")
        day = int(input("Dag: "))
        print(årstid(month, day))


if __name__ == '__main__':
    main()