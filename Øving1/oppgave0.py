def opg0_syntaksfeil():
    r = 5
    h = 8
    omkrets = 2 * 3.14 * r
    areal = 3.14 * r ** 2
    volum = areal * h
    print("Vi har en sirkel med radius", r)
    print("Omkretsen er %.2f" % omkrets)
    print("Arealet er", areal)
    print("Sylinder med høyde", h, ": volumet er", volum)
