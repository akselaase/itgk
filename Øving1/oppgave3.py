from decimal import Decimal, ROUND_HALF_UP


def opg3_kjøpmannsavrunding():
    try:
        number = Decimal(input('Gi et desimaltall: '))
        precision = Decimal(input('Antall desimalplasser: '))
    except ValueError:
        print('Enter a valid number.')
        opg3_kjøpmannsavrunding()
        return 

    print(number.quantize(10 ** -precision, rounding=ROUND_HALF_UP))


def opg3_jamesbond():
    name = input('Si et navn: ')
    parts = name.split(' ')
    print('The name is ' + parts[-1] + ', ' + name)