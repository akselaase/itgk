from oppgave0 import opg0_syntaksfeil
from oppgave1 import opg1_abstrakt_kunst
from oppgave2 import opg2_tetraeder
from oppgave3 import opg3_kjøpmannsavrunding, opg3_jamesbond


def main():
    print('Oppgave 0, retting av syntaksfeil')
    opg0_syntaksfeil()
    print('\nOppgave 1, abstrakt kunst')
    opg1_abstrakt_kunst()
    print('\nOppgave 2, tetrateder')
    opg2_tetraeder()
    print('\nOppgave 3, kjøpmannsavrunding og etternavn')
    opg3_kjøpmannsavrunding()
    print('')
    opg3_jamesbond()

    input('Press enter to quit.\n')


if __name__ == '__main__':
    main()
