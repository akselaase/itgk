from math import sqrt


def opg2_tetraeder():
    try:
        h = float(input('Høyde på tetraedet: '))
    except ValueError:
        print('That isn\'t a valid number.')
        opg2_tetraeder()
        return

    a = 3 / sqrt(6) * h
    print('Areal:', sqrt(3) * a * a)
    print('Volum:', sqrt(2) * a ** 3 / 12)
