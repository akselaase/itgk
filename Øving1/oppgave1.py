from turtle import *


def set_color(prompt: str, func_set_color):
    user_input = input(prompt)
    if user_input.count(',') == 2:
        color = []
        for part in user_input.split(','):
            try:
                component = int(part)
                if not (0 <= component <= 255):
                    raise ValueError
                else:
                    color.append(component)
            except ValueError:
                print('All color components must be integers in the range [0, 255].')
        func_set_color(tuple(color))
    else:
        # Not a RGB color
        try:
            func_set_color(user_input)
        except TurtleGraphicsError:
            print('Try a valid color.')


def opg1_abstrakt_kunst():
    # setter opp tegnevinduet
    setup(330, 330, 0, 0)
    screensize(315, 315)
    colormode(255)
    reset()
    hideturtle()
    goto(-60, 150)

    # velger farger
    set_color('Set background color: ', bgcolor)
    set_color('Set foreground color: ', color)

    # tegner den indre firkanten
    begin_fill()
    right(10)  # tilter den 10 grader nedover
    forward(200)
    right(90)
    forward(200)
    right(90)
    forward(200)
    right(90)
    forward(200)
    end_fill()
